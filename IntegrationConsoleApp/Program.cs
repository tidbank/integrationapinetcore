﻿using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace IntegrationConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {

            CallApi().Wait();

        }

        public static async Task CallApi()
        {
            //Token generert i Tidbank
            var token = "token-her";
            //URL til Integrasjons-API-et
            var url = "https://dintidbankserver/tbapiintegration";
            //Bruker vi skal hente ut tilstedestatus for
            var userId = "tidbank-brukernavn-her";

            using (var httpClient = new HttpClient())
            {
                //Legg til token i HttpClient. Dette gjør at autentisering fungerer
                //Uten denne linjen får du 401 Not authorized
                httpClient.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", token);
                //Lag instans av integrasjonsklienten og bruk httpclient som vet
                //hvordan man skal autentisere
                //Oppgi også URL til REST-API-et
                var integrationApiClient = new Client(url, httpClient);
                //Gjør et kall mot tjenesten for å hente ut tilstedestatus
                var presence = await integrationApiClient.GetPresenceAsync(userId, null, null);
                Console.WriteLine(presence.Today.Status);
                Console.ReadKey();
            }
        }
    }
}
