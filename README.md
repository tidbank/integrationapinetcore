**Tidbank Integration API sample code**

This code is just meant as a starting point and not a best practice for .NET and C#. 

You need to:
1. Provide a token (in order to authenticate)
2. Provide a Tidbank user name (to get a meaningful answer to presence status sample call)

This code uses .NET Core 3.1. This can be downloaded from Microsoft here: https://dotnet.microsoft.com/download/dotnet-core/3.1